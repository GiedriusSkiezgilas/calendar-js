const reservationsApiUrl = "http://localhost:8000/graphql";

export const getReservationTimes = (y, m, d, bool) => {
  let queryValue = "";

  if (bool) {
    queryValue = new Date();
  } else {
    queryValue = new Date(y, m, d);
  }

  return fetch(reservationsApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `{
            reservations(date: "${new Date(queryValue)}") {
                _id
                date
                reserved
                expired
            }
        }`,
    }),
  }).then((response) => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      throw new Error("Something went wrong");
    }
  });
};

export const makeReservation = (fName, lName, date) => {
  return fetch(reservationsApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `mutation {
        bookReservation(reservationInput: {
          firstName: "${fName}",
          lastName: "${lName}",
          date: "${date}",
        }) {
          _id
          firstName
          lastName
          date
          createdAt
          updatedAt
        },
      }`,
    }),
  }).then((response) => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      throw new Error("Something went wrong");
    }
  });
};
