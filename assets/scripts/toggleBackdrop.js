import {
  hideReservationsModal,
  clearReservationTimesListElement,
} from "./reservationsModal.js";
import { hideFormModal } from "./formModal.js";
import { hideSuccessModal } from "./successModal.js";

const backdrop = document.getElementById("js-backdrop");

export const toggleBackdrop = () => {
  backdrop.classList.toggle("visible");
};

const handleBackdropClick = () => {
  hideReservationsModal();
  clearReservationTimesListElement();
  hideFormModal();
  hideSuccessModal();
  toggleBackdrop();
};

backdrop.addEventListener("click", handleBackdropClick);
