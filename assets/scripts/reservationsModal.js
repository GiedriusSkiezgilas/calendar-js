import { toggleBackdrop } from "./toggleBackdrop.js";
import { getReservationTimes } from "./sendHttpRequest.js";
import { handleSelectDate } from "./formModal.js";

const reservationsModalEl = document.getElementById("js-reservations-modal");
const reservationTimesListElement = document.getElementById("js-reservations");

export const showReservationsModal = () => {
  reservationsModalEl.classList.add("visible");
  toggleBackdrop();
};

export const handleReservationsModal = (y, m, d, bool) => {
  const reservationTimeTemplate = document.getElementById(
    "js-reservation-time"
  );

  showReservationsModal();

  getReservationTimes(y, m, d, bool)
    .then((responseData) => {
      const reservationTimesList = responseData.data.reservations;

      reservationTimesList.map((reservationTime) => {
        const reservationRowElement = document.importNode(
          reservationTimeTemplate.content,
          true
        );
        const date = new Date(reservationTime.date);
        const year = date.getFullYear();
        const month = date.toLocaleString("default", { month: "long" });
        const day = date.getDate();
        const time = date.toString().slice(16, 21);
        const dateTimeElement = reservationRowElement.querySelector("p");
        const timeSelectButtonElement =
          reservationRowElement.querySelector("button");

        dateTimeElement.textContent = `${year} ${month} ${day} - ${time}`;

        if (reservationTime.expired) {
          timeSelectButtonElement.disabled = true;
          timeSelectButtonElement.textContent = "expired";
        } else if (reservationTime.reserved) {
          timeSelectButtonElement.disabled = true;
          timeSelectButtonElement.textContent = "reserved";
        } else {
          timeSelectButtonElement.textContent = "select";
          timeSelectButtonElement.addEventListener("click", () =>
            handleSelectDate(year, month, day, time)
          );
        }
        reservationRowElement.querySelector("li").id = reservationTime._id;
        reservationTimesListElement.append(reservationRowElement);
      });
    })
    .catch((error) => alert(error.message));
};

export const hideReservationsModal = () => {
  reservationsModalEl.classList.remove("visible");
};

export const clearReservationTimesListElement = () => {
  reservationTimesListElement.innerHTML = "";
};

const handleCloseModal = () => {
  hideReservationsModal();
  clearReservationTimesListElement();
  toggleBackdrop();
};

const modalCloseBtn = document.getElementById("js-modal-close-btn");
modalCloseBtn.addEventListener("click", handleCloseModal);
