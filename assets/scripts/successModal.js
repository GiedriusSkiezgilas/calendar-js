import { toggleBackdrop } from "./toggleBackdrop.js";
import { clearReservationTimesListElement } from "./reservationsModal.js";

const successModal = document.getElementById("js-success-modal");
const successModalCloseBtn = document.getElementById("js-success-close-btn");

const renderSuccessdata = (fName, lName, dateString) => {
  const fNameEl = document.getElementById("js-success-fname");
  const lNameEl = document.getElementById("js-success-lname");
  const dateEl = document.getElementById("js-success-date");
  const date = new Date(dateString);

  const y = date.getFullYear();
  const m = date.toLocaleDateString("en-us", {
    month: "long",
  });
  const d = date.getDate();
  const h = date.getHours();
  const min =
    date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();

  fNameEl.textContent = fName;
  lNameEl.textContent = lName;
  dateEl.textContent = `${y} ${m} ${d} - ${h}:${min}`;
};

export const showSuccessModal = (successDataObj) => {
  const { firstName, lastName, date } = successDataObj;

  renderSuccessdata(firstName, lastName, date);
  successModal.classList.add("visible");
};

export const hideSuccessModal = () => {
  successModal.classList.remove("visible");
};

const handleCloseSuccessModal = () => {
  hideSuccessModal();
  clearReservationTimesListElement();
  toggleBackdrop();
};

successModalCloseBtn.addEventListener("click", handleCloseSuccessModal);
