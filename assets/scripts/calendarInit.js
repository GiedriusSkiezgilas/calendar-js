import { handleReservationsModal } from "./reservationsModal.js";

let date = new Date();
const today = date.getDate();
let monthIndex = 0;

export const calendarInit = () => {
  const calendar = document.getElementById("js-dates");
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();
  const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  const firstDayOfCurrentMonth = new Date(year, month, 1);
  const daysInCurrentMonth = new Date(year, month + 1, 0).getDate();
  const daysInPreviousMonth = new Date(year, month, 0).getDate();
  const dateString = firstDayOfCurrentMonth.toLocaleDateString("en-us", {
    weekday: "long",
  });
  const filterInfoElement = document.getElementById("js-filter-info");
  filterInfoElement.innerHTML = `${date.toLocaleDateString("en-us", {
    month: "long",
  })}&nbsp;${year}`;

  const visibleDaysOfPreviousMonth = weekdays.indexOf(dateString);
  const visibleDaysOfNextMonth =
    7 - ((daysInCurrentMonth + visibleDaysOfPreviousMonth) % 7);
  const totalDatesInCalendar =
    visibleDaysOfPreviousMonth + daysInCurrentMonth + visibleDaysOfNextMonth;

  calendar.innerHTML = "";

  for (let i = 1; i <= totalDatesInCalendar; i++) {
    const dayElement = document.createElement("button");
    let dayElementValue = "";
    let isDayToday = false;
    dayElement.classList.add("calendar__date");

    if (i <= visibleDaysOfPreviousMonth) {
      dayElementValue = daysInPreviousMonth - visibleDaysOfPreviousMonth + i;
      dayElement.innerHTML = dayElementValue;
      //render previous (past, not clickable) month days
      if (monthIndex === 0) {
        dayElement.classList.add("calendar__date--past");
        // render highlited todays date if it is seen from next month
      } else if (monthIndex === 1 && today === dayElementValue) {
        isDayToday = true;
        dayElement.classList.add("today");
        dayElement.addEventListener("click", () =>
          handleReservationsModal(year, month - 1, dayElementValue, isDayToday)
        );
      } else {
        //render previous (clickable) month days
        isDayToday = false;
        dayElement.addEventListener("click", () =>
          handleReservationsModal(year, month - 1, dayElementValue, isDayToday)
        );
      }
    } else if (monthIndex === 0 && day === i - visibleDaysOfPreviousMonth) {
      // render todays day
      isDayToday = true;
      dayElementValue = i - visibleDaysOfPreviousMonth;
      dayElement.innerHTML = dayElementValue;
      dayElement.classList.add("today");
      dayElement.addEventListener("click", () =>
        handleReservationsModal(year, month, dayElementValue, isDayToday)
      );
    } else if (
      i - visibleDaysOfPreviousMonth < day &&
      i > visibleDaysOfPreviousMonth &&
      i <= totalDatesInCalendar - visibleDaysOfNextMonth
    ) {
      //render selected month not clickable dates
      dayElementValue = i - visibleDaysOfPreviousMonth;
      dayElement.classList.add("calendar__date--past", "selected-month");
      dayElement.innerHTML = dayElementValue;
    } else if (
      i > visibleDaysOfPreviousMonth &&
      i <= totalDatesInCalendar - visibleDaysOfNextMonth
    ) {
      // render selected month clickable dates
      isDayToday = false;
      dayElementValue = i - visibleDaysOfPreviousMonth;
      dayElement.classList.add("selected-month");
      dayElement.innerHTML = dayElementValue;
      dayElement.addEventListener("click", () =>
        handleReservationsModal(year, month, dayElementValue, isDayToday)
      );
    } else if (i > visibleDaysOfPreviousMonth + daysInCurrentMonth) {
      // render next month dates
      isDayToday = false;
      dayElementValue = i - visibleDaysOfPreviousMonth - daysInCurrentMonth;
      dayElement.classList.add("next-month");
      dayElement.innerHTML = dayElementValue;
      dayElement.addEventListener("click", () =>
        handleReservationsModal(year, month + 1, dayElementValue, isDayToday)
      );
    }
    calendar.append(dayElement);
  }
};

const handleClick = (direction) => {
  if (direction === "previous" && monthIndex === 1) {
    prevMonthBtn.disabled = true;
    monthIndex--;
  } else if (direction === "previous" && monthIndex > 1) {
    monthIndex--;
  } else if (direction === "next") {
    prevMonthBtn.disabled = false;
    monthIndex++;
  }

  date = new Date();

  if (monthIndex !== 0) {
    date.setMonth(new Date().getMonth() + monthIndex, 1);
  }
  calendarInit();
};

const prevMonthBtn = document.getElementById("js-button-previous");
const nextMonthBtn = document.getElementById("js-button-next");
prevMonthBtn.disabled = true;

prevMonthBtn.addEventListener("click", () => handleClick("previous"));
nextMonthBtn.addEventListener("click", () => handleClick("next"));
