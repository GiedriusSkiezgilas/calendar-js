import {
  hideReservationsModal,
  clearReservationTimesListElement,
} from "./reservationsModal.js";
import { toggleBackdrop } from "./toggleBackdrop.js";
import { showReservationsModal } from "./reservationsModal.js";
import { makeReservation } from "./sendHttpRequest.js";
import { showSuccessModal } from "./successModal.js";

const formModalElement = document.getElementById("js-form-modal");
const formModalCloseBtn = document.getElementById("js-form-close-btn");
const reservationFormElement = document.getElementById("js-form");
const errorContainerEl = document.getElementById("js-count-validation-message");

const handleClickBack = () => {
  hideFormModal();
  toggleBackdrop();
  showReservationsModal();
};

const showInputError = (input) => {
  const errorInput = input;
  const errorMessageElement = input.parentNode.querySelector(
    ".validation-message"
  );
  errorInput.classList.add("form__input--error");
  errorMessageElement.textContent = "Incorrect input value";
};

const hideInputError = (input) => {
  const errorInput = input;
  const errorMessageElement = input.parentNode.querySelector(
    ".validation-message"
  );
  errorInput.classList.remove("form__input--error");
  errorMessageElement.textContent = "";
};

const validateInput = (input) => {
  if (input.value.trim() === "") {
    showInputError(input);
    return false;
  }

  hideInputError(input);
  return true;
};

const showOnePerWeekError = (errMsg) => {
  const errorMsgEl = document.createElement("p");
  errorMsgEl.className = "error-message__text";
  errorMsgEl.textContent = errMsg;

  errorContainerEl.append(errorMsgEl);
};

const hideOnePerWeekError = () => {
  errorContainerEl.querySelector(".error-message__text").remove();
};

const submitFormhandler = (e) => {
  e.preventDefault();
  let firstNameEl = e.target.elements["first-name"];
  let lastNameEl = e.target.elements["last-name"];
  const firstName = firstNameEl.value;
  const lastName = lastNameEl.value;
  const date = e.target.elements["date"].value;
  const isFirstNameValid = validateInput(firstNameEl);
  const isLastNameValid = validateInput(lastNameEl);

  if (isFirstNameValid && isLastNameValid) {
    makeReservation(firstName, lastName, date).then((responseData) => {
      if (responseData.hasOwnProperty("errors")) {
        if (errorContainerEl.hasChildNodes())
          errorContainerEl.removeChild(errorContainerEl.firstChild);
        showOnePerWeekError(responseData.errors[0].message);
      } else {
        showSuccessModal(responseData.data.bookReservation);
        hideFormModal();
      }
    });

    firstNameEl.value = "";
    lastNameEl.value = "";
  }
  return;
};

const formatDate = (y, m, d, t) => {
  const allMonth = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const monthIndex = allMonth.indexOf(m);
  const time = t.slice(0, 2);
  const formatedDate = new Date(y, monthIndex, d, time);
  return formatedDate;
};

const showFormModal = (y, m, d, t) => {
  const formBackBtn = document.getElementById("js-form-back-btn");
  const dateInput = document.getElementById("js-reserve-time");
  const formTitleElement = document.getElementById("js-form-date");

  hideReservationsModal();
  formBackBtn.addEventListener("click", handleClickBack);

  formModalElement.classList.add("visible");
  formTitleElement.textContent = `${y} ${m} ${d} - ${t}`;

  dateInput.value = formatDate(y, m, d, t);
};

export const hideFormModal = () => {
  formModalElement.classList.remove("visible");
  if (errorContainerEl.querySelector(".error-message__text")) {
    hideOnePerWeekError();
  }
};

export const handleSelectDate = (y, m, d, t) => {
  showFormModal(y, m, d, t);
};

const handleCloseModal = () => {
  hideFormModal();
  clearReservationTimesListElement();
  toggleBackdrop();
};

formModalCloseBtn.addEventListener("click", handleCloseModal);
reservationFormElement.addEventListener("submit", (e) => submitFormhandler(e));
